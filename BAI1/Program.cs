﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BAI
{
    public class BAI_Afteken1
    {
        /// ------------------------------------------------------------
        /// <summary>
        /// Filtert een lijst. Hierbij worden alle elementen die maar
        /// 1x voorkomen verwijderd
        /// </summary>
        /// <param name="lijst">De lijst die wordt doorlopen
        /// (wordt in functie veranderd)</param>
        /// ------------------------------------------------------------
        public static void Opdr1FilterList(List<int> lijst)
        {
            // *** IMPLEMENTATION HERE *** //

            //dictionary <key: number> <value: amount of times in the list>
            Dictionary<int, int> dictionary = new Dictionary<int, int>();

            //loop trough every value in list
            foreach (int i in lijst)
            {
                //if the dictionary contains the key, add 1 to the value
                if (dictionary.ContainsKey(i))
                    dictionary[i] += 1;
                //else, create a new key of the number and set the value to 1
                else
                    dictionary.Add(i, 1);
            }

            //deletes all numbers that appear only once in the list
            foreach (KeyValuePair<int, int> pair in dictionary)
            {
                if (pair.Value == 1)
                {
                    lijst.Remove(pair.Key);
                }
            }

            foreach (int i in lijst)
            {
                Console.Write($"{i} ");
            }
        }


        /// ------------------------------------------------------------
        /// <summary>
        /// Maakt een queue van de getallen 1 t/m 50 (in die volgorde
        /// toegevoegd)
        /// </summary>
        /// <returns>Een queue met hierin 1, 2, 3, .., 50</returns>
        /// ------------------------------------------------------------
        public static Queue<int> Opdr2aQueue50()
        {
            // *** IMPLEMENTATION HERE *** //
            int[] arr = Enumerable.Range(1, 50).ToArray();
            Queue<int> q = new Queue<int>(arr);
            return q;
        }


        /// ------------------------------------------------------------
        /// <summary>
        /// Haalt alle elementen uit een queue. Voegt elk element dat
        /// deelbaar is door 4 toe aan een stack
        /// </summary>
        /// <param name="queue">De queue die uitgelezen wordt</param>
        /// <returns>De stack met hierin de elementen die deelbaar zijn
        /// door 4</returns>
        /// ------------------------------------------------------------
        public static Stack<int> Opdr2bStackFromQueue(Queue<int> queue)
        {
            Stack<int> stack = new Stack<int>();

            // *** IMPLEMENTATION HERE *** //
            while (queue.Count > 0)
            {
                var x = queue.Dequeue();
                if (x % 4 == 0)
                    stack.Push(x);
            }
            return stack;
        }


        /// ------------------------------------------------------------
        /// <summary>
        /// Maakt een stack met hierin unieke random getallen</summary>
        /// <param name="lower">De ondergrens voor elk getal (inclusief)</param>
        /// <param name="upper">De bovengrens voor elk getal (inclusief)</param>
        /// <param name="count">Het aantal getallen</param>
        /// <returns>De stack met unieke random getallen</returns>
        /// ------------------------------------------------------------
        public static Stack<int> Opdr3RandomNumbers(int lower, int upper, int count)
        {
            Stack<int> stack = new Stack<int>();
            Random random = new Random();
            // *** IMPLEMENTATION HERE *** //
            Dictionary<int, bool> dictionary = new Dictionary<int, bool>();
            while (stack.Count < count)
            {
                int x = random.Next(lower, upper + 1);
                if (!dictionary.ContainsKey(x))
                { 
                    dictionary.Add(x, true);
                    stack.Push(x);
                }
            }
            return stack;
        }


        /// ------------------------------------------------------------
        /// <summary>
        /// Drukt een IEnumerable (List, Stack, Queue, ..) van getallen
        /// af naar de Console
        /// <param name="enu">De IEnumerable om af te drukken</param>
        /// ------------------------------------------------------------
        static void PrintEnumerable(IEnumerable<int> enu)
        {
            foreach (int i in enu)
            {
                Console.Write($"{i} ");
            }

            Console.WriteLine();
        }


        static void Main(string[] args)
        {
            List<int> list;
            Queue<int> queue;
            Stack<int> stack;

            Console.WriteLine();
            Console.WriteLine("=== Opdracht 1 : FilterList ===");
            list = new List<int>() { 1, 3, 5, 7, 3, 8, 9, 5 };
            PrintEnumerable(list);
            Opdr1FilterList(list);
            PrintEnumerable(list);

            Console.WriteLine();
            Console.WriteLine("=== Opdracht 2 : Stack / Queue ===");
            queue = Opdr2aQueue50();
            PrintEnumerable(queue);
            stack = Opdr2bStackFromQueue(queue);
            PrintEnumerable(stack);

            Console.WriteLine();
            Console.WriteLine("=== Opdracht 3 : Random number ===");
            stack = Opdr3RandomNumbers(100, 150, 10);
            PrintEnumerable(stack);
            stack = Opdr3RandomNumbers(10, 15, 6);
            PrintEnumerable(stack);
            stack = Opdr3RandomNumbers(10_000, 50_000, 40_001);
        }
    }
}